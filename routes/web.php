<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Bulletin Board
Route::get('/bulletin', 'ActivityController@index');

// Jobs
Route::get('/alljobs', 'JobController@allJobs');
Route::get('/addjob', 'JobController@createJob');
Route::post('/addjob', 'JobController@storeJob');
Route::delete('/deletejob/{id}', 'JobController@destroyJob');
Route::get('/editjob/{id}', 'JobController@editJob');
Route::patch('/editjob/{id}', 'JobController@updateJob');
Route::get('/filterbyjob', 'JobController@filterByJob');

// Activities
Route::get('/allactivities', 'ActivityController@allActivity');
Route::get('/addactivity', 'ActivityController@createActivity');
Route::post('/addactivity', 'ActivityController@storeActivity');
Route::delete('/deleteactivity/{id}', 'ActivityController@destroyActivity');
Route::get('/editactivity/{id}', 'ActivityController@editActivity');
Route::patch('/editactivity/{id}', 'ActivityController@updateActivity');



// Admin
Route::get('/showattendees/{id}', 'ActivityController@showAttendees');
Route::get('/changerole/{id}', 'UserController@changeRole');
Route::get('/allusers', 'UserController@index');
Route::get('/changestatus/{id}', 'UserController@changeStatus');

Route::get('/announcements', 'AnnouncementController@showAnnouncements');
Route::get('/addannouncement', 'AnnouncementController@createAnnouncement');
Route::post('/addannouncement', 'AnnouncementController@storeAnnouncement');
Route::get('/editannouncement/{id}', 'AnnouncementController@editAnnouncement');
Route::patch('/updateannouncement/{id}', 'AnnouncementController@updateAnnouncement');
Route::delete('/deleteannouncement/{id}', 'AnnouncementController@destroyAnnouncement');
Route::patch('/editannouncement/{id}', 'AnnouncementController@storeAnnouncement');

Route::delete('/deleteattendee/{id}', 'UserController@destroyAttendee');
Route::get('/userjobs', 'UserController@showUserJobs');

// User
Route::get('/createattendance', 'ActivityController@createAttendance');
Route::post('/createattendance', 'ActivityController@storeAttendance');
Route::post('/applyforjob', 'UserController@storeUserJob');
Route::get('/applyforjob', 'UserController@applyjob');



// 