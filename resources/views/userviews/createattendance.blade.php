@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Create Attendance</h1>

<div class="col-lg-4 offset-lg-4">
	
	<form action="/createattendance" method="POST" enctype="multipart/form-data">	
		@csrf
		<div class="form-group">
			<label for="activity_id">Activity</label>
			<select name="activity_id" class="form-control">
				@foreach($activities as $activity)
					<option value="{{$activity->id}}" {{in_array($activity->id, $attended) ? "disabled" : ""}}>{{$activity->title}}</option>
				@endforeach
			</select>
		</div>
		<button class="btn btn-success" type="submit">Submit Attendance</button>
	</form>

</div>
@endsection