@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Apply for Job</h1>

<div class="col-lg-4 offset-lg-4">
	
	<form action="/applyforjob" method="POST" enctype="multipart/form-data">	
		@csrf
		<div class="form-group">
			<label for="job_id">Jobs</label>
			<select name="job_id" class="form-control">
				@foreach($jobs as $job)
					<option value="{{$job->id}}">{{$job->title}}</option>
				@endforeach
			</select>
		</div>
		<button class="btn btn-success" type="submit">Submit Application</button>
	</form>

</div>
@endsection