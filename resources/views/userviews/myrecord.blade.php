@extends('layouts.app')
@section('content')


<div class="container d-flex justify-content-center">
	<div class="row">
		<div class="">
			<div class="row w-100">
				<div class="col-lg-12 p-3 my-2">
					<div class="card">
						<div class="card-body text-center">
							@foreach($users as $user)
							<tr>
								<h1>My Attendance Record<br></h1>					
								@foreach($user->activities as $activity)
									<th>{{$activity->title}}</th>
									<th>{{$activity->date}}<br></th>
								@endforeach
							</tr>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>

@endsection