@extends("layouts.app")
@section('content')

<h1 class="text-center py-5">Bulletin Board</h1>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body text-center">
					<h2>Announcement</h4>
					<h3>
						{{-- @foreach($announcements as $announcement)
						@if($announcements->announcement != []) --}}
							
								{{$announcement->announcement}}
							
						{{-- @else
							<h3>No Announcement</h3>
						@endif
						@endforeach --}}
					</h3>
				</div>
			</div>
			<div class="row w-100">
				
				@foreach($activities as $activity)
					<div class="col-lg-4 p-3 my-2">
						<div class="card">
							<div class="card-body">
								<img class="card-img-top" src="{{asset($activity->imgPath)}}" alt="Nothing" height="300px">
								<h2 class="card-title">{{$activity->title}}</h2>
								<p class="card-text">{{$activity->description}}</p>
								<p class="card-text">{{$activity->venue}}</p>
								<p class="card-text">{{$activity->date}}</p>
							</div>
							@if(Auth::user()->role_id == 2 && Auth::user()->status_id == 1)
							<a href="/showattendees/{{$activity->id}}" class="btn btn-info">Show Attendees</a>
							@elseif(Auth::user()->role_id == 1 && Auth::user()->status_id == 1)
							<a href="/editactivity/{{$activity->id}}" class="btn btn-success">Edit</a>
							<form action="/deleteactivity/{{$activity->id}}" method="POST">
								@csrf
								@method('DELETE')
								<button class="btn btn-danger" type="submit">DELETE</button>
							</form>
							@endif
						</div>
					</div>

				@endforeach
			</div>
		</div>
	</div>
</div>

@endsection