@extends('layouts.app')
@section('content')


<div class="container d-flex justify-content-center">
	<div class="row">
		<div class="">
			<div class="row w-100">
				<div class="col-lg-12 p-3 my-2">
					<div class="card">
						<div class="card-body text-center">
							<h3>Add Announcement</h3>
							<form action="/addannouncement" method="POST">								
									@csrf
									<textarea name="announcement">
									</textarea>						
								<button class="btn btn-success" type="submit">Add</button>
							</form>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>

@endsection