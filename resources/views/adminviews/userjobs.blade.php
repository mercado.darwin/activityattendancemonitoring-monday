@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Jobs</h1>
<div class="col-lg-10 offset-lg-2">
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th>User</th>
				<th>Job Title</th>
				<th>status</th>
				<th>Actions</th>				
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
			<tr>
				<th>{{$user->name}}</th>
				@foreach($user->jobs as $job)
				<th>{{$job->title}}</th>
				<th></th>
				<th>
									
				</th>
				@endforeach
			</tr>			
			@endforeach
		</tbody>
	</table>
</div>
@endsection