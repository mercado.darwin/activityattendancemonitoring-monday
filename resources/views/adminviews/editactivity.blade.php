@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Edit Activity Form</h1>

<div class="col-lg-4 offset-lg-4">
	<form action="/editactivity/{{$activity->id}}" method="POST" enctype="multipart/form-data">
		@csrf
		@method('PATCH')
		<div class="form-group">
			<label for="title">Title</label>
			<input type="text" name="title" class="form-control" value="{{$activity->title}}">
		</div>
		<div class="form-group">
			<label for="description">Description</label>
			<input type="text" name="description" class="form-control" value="{{$activity->description}}">
		</div>
		<div class="form-group">
			<label for="venue">Venue</label>
			<input type="text" name="venue" class="form-control" value="{{$activity->venue}}">
		</div>
		<div class="form-group">
			<label for="date">Date</label>
			<input type="text" name="date" class="form-control" value="{{$activity->date}}">
		</div>
		<button class="btn btn-warning" type="submit">Edit Activity</button>
	</form>
</div>
@endsection