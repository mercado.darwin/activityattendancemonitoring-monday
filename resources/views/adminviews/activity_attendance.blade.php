@extends('layouts.app')
@section('content')


<div class="container d-flex justify-content-center">
	<div class="row">
		<div class="">
			<div class="row w-100">
				<div class="col-lg-12 p-3 my-2">
					<div class="card">
						<div class="card-body text-center">
							@foreach($activities as $activity)
							<tr>
								<img class="card-img-top img img-responsive full-width" src="{{asset($activity->imgPath)}}" alt="Nothing" style="height:200px; width:200px">
								<h1>{{$activity->title}}<br></h1>					
								@foreach($activity->users as $user)
								<div class="d-flex justify-content-center align-items-center text-center">
									
									<th>{{$user->name}}</th>
									<th>
									@if($user->role_id = 1)
										
										<form action="/deleteattendee/{{$user->id}}" method="POST">
											@csrf
											@method('DELETE')
											<input type="hidden" name="activity_id" value="{{$activity->id}}">
											<button type="submit" class="btn btn-danger">Delete</button>
										</form>
									@endif
									</th>
								</div><br>
									
								@endforeach
							</tr>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>

@endsection