<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Announcement;
use Session;

class AnnouncementController extends Controller
{
    public function createAnnouncement(){
    	$announcements = Announcement::all();

    	return view('adminviews.addannouncement', compact('announcements'));
    }

    public function editAnnouncement($id){
        $announcement = Announcement::find($id);

        return view('adminviews.editannouncement', compact('announcement'));
    }

    public function destroyAnnouncement($id){
        $announcement = Announcement::find($id);
        $announcement->delete();

        Session::flash("message", "$announcement->announcement has been deleted");
        return redirect('/announcements');
    }

    public function updateAnnouncement($id, Request $req){
        $announcement = Announcement::find($id);

        $rules = array(
            "announcement" => "required",
        );

        $this->validate($req, $rules);

        // capture
        $announcement->announcement = $req->announcement;
        $announcement->save();

        Session::flash("message", "$announcement->announcement has been updated");
        return redirect('/announcements');
    }

    public function storeAnnouncement(Request $req){
    	$rules = array(
            "announcement" => "required",
        );

        $this->validate($req, $rules);

        // capture
        $announcement = new Announcement;
        $announcement->announcement = $req->announcement;
        $announcement->save();

        Session::flash("message", "$announcement->announcement has been added");
        return redirect()->back();
    }

    public function showAnnouncements(){
        $announcements = Announcement::all();

        return view('adminviews.announcements', compact('announcements'));
    }
}
