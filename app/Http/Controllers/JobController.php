<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Job;
use Session;

class JobController extends Controller
{
    public function createJob(){
    	$jobs = Job::all();

    	return view('adminviews.addjob', compact('jobs'));
    }

    public function storeJob(Request $req){
    	$rules = array(
    		"title" => "required",
    		"description" => "required"
    	);

    	$this->validate($req, $rules);

    	// capture
    	$job = new Job;
    	$job->title = $req->title;
    	$job->description = $req->description;
    	$job->save();

    	Session::flash("message", "$job->title has been added");
    	return redirect()->back();
    }

    public function editJob($id){
    	$job = Job::find($id);

    	return view('adminviews.editjob', compact('job'));
    }  


    public function updateJob($id, Request $req){
    	$job = Job::find($id);

    	$rules = array(
    		"title" => "required",
    		"description" => "required"
    	);

    	$this->validate($req, $rules);

    	// capture
    	$job->title = $req->title;
    	$job->description = $req->description;
    	$job->save();
    	return redirect('/alljobs');
    }

    public function destroyJob($id){
    	$job = Job::find($id);
    	$job->delete();

    	return redirect()->back();
    }

    public function allJobs(){
    	$jobs = Job::all();

    	return view('adminviews.jobs', compact('jobs'));
    }

    public function filterByJob($id){
    	$jobs = User::where('id', $id)->get();

    	return view('adminviews.jobs', compact('jobs'));
    }
}
