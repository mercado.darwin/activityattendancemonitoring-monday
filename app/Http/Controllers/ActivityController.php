<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Activity;
use \App\User;
use \App\Announcement;
use Auth;
use Session;

class ActivityController extends Controller
{
    public function index(){
    	$activities = Activity::orderBy('date', 'desc')->get();
        $announcement = Announcement::find(1);
        // $announcements = Announcement::where('status', 2)->get();
    	return view('bulletin', compact('activities', 'announcement'));
    }

    public function createActivity(){
    	$activities = Activity::all();

    	return view('adminviews.addactivity', compact('activities'));
    }

    public function storeActivity(Request $req){
    	$rules = array(
    		"title" => "required",
    		"description" => "required",
    		"date" => "required",
            "venue" => "required",
            "imgPath" => "required|image|mimes:jpeg,jpg,png,gif,tiff,tif,webp,bitmap"
    	);

    	$this->validate($req, $rules);

    	// capture
    	$activity = new Activity;
    	$activity->title = $req->title;
    	$activity->description = $req->description;
    	$activity->date = $req->date;
    	$activity->venue = $req->venue;

        // image handling
        $image = $req->file('imgPath');
        // We'll rename the image
        $image_name = time().".".$image->getClientOriginalExtension();

        $destination = "images/"; //corresponds to the public images directory

        $image->move($destination, $image_name);

        $activity->imgPath = $destination.$image_name;

        
        $activity->save();

    	Session::flash("message", "$activity->title has been added");
    	return redirect('/allactivities');
    }

    public function destroyActivity($id){
    	$activity = Activity::find($id);
    	$activity->delete();

    	return redirect()->back();
    }

    public function createAttendance(){
        $activities = Activity::orderBy('date', 'desc')->get();
        $myActivities = Auth::user()->activities()->get();
        $attended = [];

        foreach($myActivities as $activity){
            $attended[] = $activity->id;
        }

        // dd($attended);

        return view('userviews.createattendance', compact('activities','myActivities', 'attended'));
    }


    public function storeAttendance(Request $req){
        $user = Auth::user()->id;
        $activities = Activity::find($req->input('activity_id'));
        $activities->users()->attach($user);
        $activities->save();

        return redirect('/bulletin');
    } 

	public function allActivity(){
    	$activities = Activity::orderBy('date', 'desc')->get();
        $users = User::all();

        $activitiesWithUsers = $users->pluck('activity_id')->toArray();    	

    	return view('adminviews.activities', compact('activities', 'activitiesWithUsers'));
    }    

    public function editActivity($id){
    	$activity = Activity::find($id);    	

    	return view('adminviews.editactivity', compact('activity'));
    }

    public function updateActivity($id, Request $req){
        $activity = Activity::find($id);

        $rules = array(
            "title" => "required",
            "description" => "required",
            "date" => "required"
        );

        $this->validate($req, $rules);

        $activity->title = $req->title;
        $activity->description = $req->description;
        $activity->date = $req->date;

        $activity->save();
        return redirect('/allactivities');
    }

    public function showAttendees($id){
        $activities = Activity::where('id', $id)->get();
        
        return view('adminviews.activity_attendance', compact('activities'));
    }

    public function viewMyRecord(){
        $users = User::where('id', Auth::user()->id)->get();
        
        return view('userviews.myrecord', compact('users'));
    }

}
